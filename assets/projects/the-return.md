## What Happened?

Before I took my website down in (2015?) it was running as a WordPress site, functioning pretty much as a blog. It was actually a pretty nifty setup I think. First, I would tag posts as being 'photography', 'modding', or otherwise. Then I would filter posts by tag depending on which section of the site you were viewing. The site used a custom WordPress theme and was fairly easy to keep up-to-date.

Around 2015 I needed to cut costs, and I also hadn't been updating my website very frequently. I thought I'd save some money by removing the site and backing up all the contents. At the time, I didn't fully understand how the setup worked, and I didn't realize that the MySQL database files weren't stored alongside the WordPress files; they were lost during the shutdown (though you can still find some of it on the Wayback Machine, which is super cool! Thanks archive.org!)

## What's New With The Site?

This site is built with the Nuxt framework, which is a node-based javascript framework for building sites using Vue. I chose Nuxt because it gives me the benefit of being able to generate static content which is pre-rendered but still enable the use of Vue, which is a pretty flexible front-end framework that I already know.

Last time I tried to get my website back up, I had planned on using Nuxt 1.x, but the Nuxt team was in the middle of upgrading to a new major version (2.x) and many of the features were in flux. I decided to wait until it stabilized.

This time, the Nuxt team is well into developing Nuxt V3, and it isn't quite finished. Yesterday as of writing this, Nuxt V3 release candidate was [announced](https://twitter.com/nuxt_js/status/1498578420183273474?cxt=HHwWhMC4scTIgswpAAAA). Instead of waiting for V3, I decided to create this project using the [Nuxt V3 Bridge](https://v3.nuxtjs.org/getting-started/bridge/). I would have gone straight to v3 beta, but I wanted something a bit more stable and I wanted to generate a static project (which at the time v3 beta did not support except via bridge).

The setup was a bit strange because it's written from the perspective of updating an existing nuxt v2 server, but I just started by following the nuxt v2 project setup then installing bridge.

For articles, I didn't want to type up everything into vue components directly. I created a file ```content.js``` which has a simple data structure. It has a special key (```all```) which is a list of article slugs. Then, each slug corresponds to an object in ```content``` which holds metadata including the name of the article, the date of the article, some preview text, and others.

```
export default {
    all: ["the-return"],
    "the-return": {
        "index": 0,
        "slug": "the-return",
        "title": "The Return",
        "subtitle": "A New Beginning",
        "date": "02/28/2022",
        "preview": `It's been several years since I took down the old instance of my website.

          In this post I will detail why I took down the website, what I've been up to
          these last few years, and what's next for morerunes.com.`
    }
}
```

Next, each slug also corresponds to a Markdown file where the content of the article is actually written. Markdown benefits from being a good compromise between human-readable and machine-readable. It easily transforms to HTML, and a vue component can take HTML as its content (this is the basis of the [@nuxt/content plugin](https://content.nuxtjs.org), which is not yet supported on nuxt v3 or bridge). I figured that at some point it may be unsustainable to keep adding new markdown files and json entries for articles, and when it seems I'm going that direction I'll look to solutions like nuxt content v3, or other solutions.

In the assets folder we'll then have `the-return.md`

Nuxt supports hot module reloading out of the box, which is pretty nice for development. Even though I am building a static website, I still have the ability to run a dev server which updates the content on the screen as I make changes in my editor.

The site is being deployed to [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/), a free (within limits) static site hosting service provided to all users. When I push any commits to the [master branch](https://gitlab.com/morerunes/www-morerunes-com) of the project, a pipeline builds the project and uploads the updated content to Gitlab Pages, and the website is updated automatically.

## What Else?

Aside from the site, I've started (or resumed) working on a few projects recently which I feel could use a proper home. Here are a few of them:

### delectamentum-mud-server

[delectamentum-mud-server](https://gitlab.com/morerunes/delectamentum-mud-server) is a game engine that my friend Phil Martinez and I started working on in 2016 written in node. I didn't really have grand plans for writing any games in it, but I felt it was a good choice for learning and working with various data structures and the javascript object model. Node seemed like a good choice for a multi-user game engine because it runs using an event loop and supports a large number of connections and interactions out of the box.

The challenge with node was always going to be with the object model: when development began for the server, ECMAScript 6 (i.e. Javascript ES6) was fresh and new, having just been introduced in 2015. Classes had just been introduced, and while they made it a lot easier to deal with Objects and inheritence and whatnot, the support wasn't quite 100% there. I can't recall specifically what issues we ran into at the time, but it was slightly messy and unintuitive to write objects the [traditional way](https://hacks.mozilla.org/2015/07/es6-in-depth-classes/), while the new way didn't support everything we needed yet either.

Eventually life got in the way and development on the project ceased. A few weeks ago I started working on the server again, because I was inspired to try to write something in [Unity](https://unity.com), and I decided I would try to interface it with the mud server. Development is ongoing, though slowed down due to the release of Elden Ring :)

### Fable: The Lost Chapters Mods

An unlikely development, but it's happening. I have pulled up my old tools, and I've been getting back into Fable modding. I'm not ready to dive into any developments yet, but I've put a new food item into the game, pending some small tweaks. Expect to see some content in the near future, and in the mean-time check out [Avarice on YouTube](https://www.youtube.com/channel/UCrar6lN4P4tNeGG4UVt9CzQ) to see some recent activity from the community!

![Screenshot of a tub of ice cream in Fable](/images/post/fable-preview-1.jpeg)

### The Craftsman's Candyland

Long ago I assumed this project was dead, decomposed, and bearing new fruit of working full time jobs and watching Netflix. However, that was a wack thing to assume and there's no reason I shouldn't still develop it! The biggest issue I ran into was that I wasn't sure where to take it next - I had the setup, I had ideas about how I wanted the mod to end, and what mechanics would be involved, but no concrete start-to-end vision.

I've done some brainstorming, and I know where I want to take Candyland. I plan on writing about this in a future post, because it really deserves a write-up of its own, but for now please enjoy the first progress update [since 2013](https://web.archive.org/web/20131227160043/http://morerunes.com/?p=244).

Also, I'm not super upset because I know I can do better, but I did lose some assets for the mod at some point - specifically the Craftsman's tower and the rock candy trees.

![Screenshot of a candy mine](/images/post/candyland-preview-1.jpeg)

### Rune Factory

I went a few years without making music, but I also have amassed quite a backlog of unfinished or unreleased tracks by this point. I've resumed working on music, and while it hasn't been as much of a focus very recently I definitely want to get more music released in the near future. I don't really have anything I'm ready to publish quite yet, but it'll go up on Bandcamp when it's ready, and get its own blog post.

## What's Next?

At least for the website, I have a couple of options:

- I could wait for [@nuxt/content v3](https://twitter.com/Atinux/status/1494289545713655809) which is supposedly coming soon. The workflow of content is very similar to what I am currently using for generating this website, but it would have the benefit of not being slightly-hacked-together.

- I could plug in some sort of database and move away from static content. This is appealing from the perspective of trying new things and practicing other technologies, but involves running servers which can get costly and exposes me to more security risks (what if I get really sleepy and don't update the server for two years?)

- Finally, I could continue to build upon my slightly-customized version of Nuxt. If it works, why fix it?

If you read all this, my sincerest thanks!

## TL;DR

This website was a fun technical project, and I've got other projects in the works so keep an eye out for new content and updates!
