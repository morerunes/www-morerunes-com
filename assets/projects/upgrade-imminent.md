## Upcoming Site Upgrade

I went looking, and the [Nuxt Content](https://nuxt.com/modules/content) supports [Nuxt 3](https://nuxt.com/), so I plan to upgrade to that at some point.

Currently this site is built using nuxt-bridge with Nuxt 2.0. I'm excited to see that Nuxt Content is still being supported to enable static site generation!
