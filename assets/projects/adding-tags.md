# Site Update: Adding Tags

## We got tags now

I've added support for tags on my posts to make them easier to browse in the future.
One of the things I find really cool with the particular combination of Nuxt and Content my site is built with
is the ability for me to dynamically build the final data structure that drives the static content.
Since the source for generating the static content is from a javascript file, I can put all the post data in a
big object then run through the object and make more structures in it before exporting. For tags, it looks (mostly) like this:

```javascript
contentMap.all.forEach(article => {
    contentMap[article].tags.forEach(tag => {
        if (!contentMap.tags[tag]) {
            contentMap.tags[tag] = [];
        }

        contentMap.tags[tag].push(article);
    });
});

// sort tag groups by index

Object.values(contentMap.tags).forEach(tag => {
    tag.sort((a, b) => {
        if (contentMap[a].index > contentMap[b].index) {
            return 1;
        } else return -1;
    });
});
```

This copies each article into each list of articles for the tags, then sorts by index. Probably not the most efficient or
fastest way to solve this problem, but it was conceptually easy and I don't have to worry about performance just yet.

The transformation basically looks as follows:

```javascript
let contentInput = {
    all: [ 'article-one', 'article-two' ],
    'article-one': { tags: [ 'stuff', 'things' ], title: 'Stuff and things!', index: 1 },
    'article-two': { tags: [ 'things' ], title: 'Just Things', index: 2 }
};

// do magic

contentOutput = {
    all: [ 'article-one', 'article-two' ],
    'article-one': { tags: [ 'stuff', 'things' ], title: 'Stuff and things!' },
    'article-two': { tags: [ 'things' ], title: 'Just Things'},
    tags: {
        stuff: [ 'article-one' ],
        things: [ 'article-two', 'article-one' ]
    }
};
```

I'm sort of curious to see how far I can take the site using this approach, if I use a lot of tags the output could get
a bit heavy, but at least for a while I'm thinking this should work just fine.

Now that I'm previewing this article before publishing, I'm realizing the site could definitely benefit from adding syntax
highlighting for code blocks. I'll look into this a bit more before jumping on anything, but for now I think I'm happy with
this particular update.
