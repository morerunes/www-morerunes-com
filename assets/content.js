const contentMap = {
    all: [
        'upgrade-imminent',
        'adding-tags',
        "life-updates-june-2023",
        "the-return"
    ],
    "the-return": {
        "index": 0,
        "slug": "the-return",
        "title": "The Return",
        "subtitle": "A New Beginning",
        tags: ['tech', 'blog', 'mods'],
        date: "February 28, 2022 12:00:00",
        image: '/images/post/candyland-preview-1.jpeg',
        "preview": `It's been several years since I took down the old instance of my website.

          In this post I will detail why I took down the website, what I've been up to
          these last few years, and what's next for morerunes.com.`
    },
    "life-updates-june-2023": {
        index: 1,
        slug: "life-updates-june-2023",
        title: "Life Updates - June 2023",
        subtitle: "Total Refresh",
        tags: ['blog', 'queer'],
        date: "June 4, 2023 12:00:00",
        image: '/images/post/transformation.jpeg',
        preview: `In the spirit of pride month, and also because everything online is permanent anyway,
          it's time for me to just come out and say it. I'm trans. I've been living in the closet my whole life
          and not only am I sick of it, living closeted slowly eroded my ability to do anything other than
          the bare minimum to get by, and I completely stopped doing creative projects because I didn't have the energy.
        `
    },
    'adding-tags': {
        index: 2,
        slug: 'adding-tags',
        title: 'Site Update: Adding Tags',
        subtitle: 'We got categories now',
        tags: ['tech'],
        date: 'July 16, 2023 12:00:00',
        image: '/images/post/tag-cloud.png',
        preview: `Just a quick site update, I'm adding in tags for the new site to make it easier to find things later.`
    },
    'upgrade-imminent': {
        index: 3,
        slug: 'upgrade-imminent',
        title: 'Site Update: Upcoming Upgrade',
        subtitle: 'When will it happen? Nobody really knows...',
        tags: ['tech'],
        date: 'January  29, 2025 12:00:00',
        image: 'https://raw.githubusercontent.com/nuxt/content/main/.github/social-card.png',
        preview: `Mostly just updating the 'last update' but also intending to upgrade the site at some point in the near future.`
    }
};

// add tag groups

if (!contentMap.tags) {
    contentMap.tags = {};
}

contentMap.all.forEach(article => {
    if (!contentMap[article].tags) {
        contentMap[article].tags = [];
    }

    contentMap[article].tags.forEach(tag => {
        if (!contentMap.tags[tag]) {
            contentMap.tags[tag] = [];
        }

        contentMap.tags[tag].push(article);
    });
});

// sort tag groups by index

Object.values(contentMap.tags).forEach(tag => {
    tag.sort((a, b) => {
        if (contentMap[a].index < contentMap[b].index) {
            return 1;
        } else return -1;
    });
});

export default contentMap;
