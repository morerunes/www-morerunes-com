import { defineNuxtConfig } from '@nuxt/bridge'
import RSS from 'rss';
import fs from 'fs';
import content from './assets/content'

export default defineNuxtConfig({
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  typescript: {
    shim: false
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'morerunes.com',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'alternate', type: 'application/rss+xml', title: 'RSS', href: 'https://morerunes.com/rss.xml' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/global.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ],

  styleResources: {
    scss: ['assets/global.scss']
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, { isDev }) {
      // -- probably not the best place for this, but for lack of a better place -- //
      let feedOptions = {
        title: 'morerunes.com',
        description: 'Coding, Music, and More!',
        feed_url: 'https://morerunes.com/rss.xml',
        site_url: 'https://morerunes.com/',
        copyright: 'Aster Echols'
      };

      let feed = new RSS(feedOptions);

      for (let i = 0; i < Math.min(11, content.all.length); i++) {
        let article = content[content.all[i]];
        feed.item({
          title: article.title,
          description: article.preview,
          url: `https://morerunes.com/projects/${content.all[i]}`
        });
      }

      fs.writeFile('./static/rss.xml', feed.xml(), 'ascii', err => {
        if (err) {
          return console.error(err);
        }

        console.log('rss.xml updated');
      });

      // build date should show what day it was built but not more specific
      let buildDate = Date.now();
      buildDate -= (buildDate % 86400000); // reset to beginning of day
      buildDate += 43200000; // set to middle of current day

      fs.writeFile('./static/meta.json',
        JSON.stringify({buildDate}),
        'ascii', err => {
          if (err) {
            return console.error(err);
          }

          console.log('meta.json updated');
        }
      )
      // -- end ridiculous kludge -- //

      // .md -> html -> text
      config.module.rules.push({
        test: /\.md$/,
        loader: ['raw-loader', 'markdown-loader']
      });
    }
  },

  // generate: {
  //   exclude: [/^\/admin/] // exclude every URL starting with "/admin"
  // },

  router: {
    base: '/'
  }
})
